import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import Navbar from "./Components/Navbar";
import Footer from "./Components/Footer";
import DetailMoviePage from "./pages/DetailMoviePage";

function App() {
  return (
    <Router>
      <Navbar />

      <Switch>
        <Route exact path="/detail-movie/:id" component={DetailMoviePage} />
        <Route exact path="/" component={HomePage} />
      </Switch>
      <Footer />
    </Router>
  );
}
export default App;
