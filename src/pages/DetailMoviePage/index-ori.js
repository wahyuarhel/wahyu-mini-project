import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  Link,
} from "react-router-dom";
import { Button, Container, Jumbotron } from "reactstrap";

import { getDetailMovieById } from "../../Redux/actions/HomePage";
import characterPage from "./DetailMovieTab/characterPage";
import overviewPage from "./DetailMovieTab/overviewPage";
import reviewPage from "./DetailMovieTab/reviewPage";
import { imgUrl } from "../../utils/constants";

const DetailMoviePage = ({ movie, getDetailMovieById }) => {
  const { id } = useParams();

  useEffect(() => {
    getDetailMovieById(id);
  }, []);

  return (
    <Router>
      <Jumbotron
        className="mb-3"
        style={{
          backgroundImage: `url(${imgUrl}/${movie.backdrop_path})`,
          backgroundSize: "cover",
        }}
      >
        <img src="" alt="" />
        <div className="text-white">
          <h1>{movie.title}</h1>
          <span>{movie.vote_average}/10</span>{" "}
          <span>{movie.vote_count} votes</span>
          <hr className="my-2" />
          <p className="lead">{movie.tagline}</p>
          <hr className="my-2" />
          <p className="lead">
            <Button color="primary mr-4">Watch Trailer</Button>
            <Button color="primary">Add To Watch List</Button>
          </p>
        </div>
      </Jumbotron>
      <Container>
        <div className="detail-page-content">
          <Button
            className="mr-3 rounded-pill"
            color="primary"
            outline
            size="sm"
          >
            <Link to="/">Overview</Link>
          </Button>
          <Button
            className="mr-3 rounded-pill"
            color="primary"
            outline
            size="sm"
          >
            <NavLink to="/character">Characters</NavLink>
          </Button>
          <Button
            className="mr-3 rounded-pill"
            color="primary"
            outline
            size="sm"
          >
            <NavLink to="/review">Review</NavLink>
          </Button>
        </div>

        <Switch>
          <Route path="/character" component={characterPage} />
          <Route path="/review" component={reviewPage} />
          <Route path="/" component={overviewPage} />
        </Switch>
      </Container>
    </Router>
  );
};

const mapStateToProps = (state) => {
  return {
    movie: state.homePage.movie,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDetailMovieById: (id) => dispatch(getDetailMovieById(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailMoviePage);
