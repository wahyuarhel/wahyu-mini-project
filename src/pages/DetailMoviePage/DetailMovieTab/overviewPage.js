import React from "react";
export default function overviewPage() {
  return (
    <div className="mt-3 detail-page-content">
      <h3>Synopsis</h3>
      <p>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat
        laboriosam esse adipisci veniam tempora suscipit reprehenderit quos
        maiores repudiandae inventore delectus blanditiis maxime iste omnis in,
        vitae doloribus corporis animi!
      </p>
      <hr />
      <h3>Movie Details</h3>
      <table>
        <tr>
          <th>Release Date</th>
          <td> : </td>
          <td>January 5, 2021</td>
        </tr>
        <tr>
          <th>Director</th>
          <td> : </td>
          <td>Jacob</td>
        </tr>
        <tr>
          <th>Featured Song</th>
          <td> : </td>
          <td>A Million times</td>
        </tr>
      </table>
      <hr />
    </div>
  );
}
