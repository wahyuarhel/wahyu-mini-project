import React from "react";
import { Row } from "reactstrap";
export default function characterPage() {
  return (
    <div className="mt-3 detail-page-content">
      <Row className="d-flex">
        <div className="mx-2">
          <img
            className="img-card-character"
            src="https://www.themoviedb.org/t/p/w138_and_h175_face/amOshiwsbyIyvkhm9QK48xuafyH.jpg"
            alt="character"
          />
          <p className="">Character Name</p>
        </div>
        <div>
          <img
            className="img-card-character"
            src="https://www.themoviedb.org/t/p/w138_and_h175_face/amOshiwsbyIyvkhm9QK48xuafyH.jpg"
            alt="character"
          />
          <p className="">Character Name</p>
        </div>
      </Row>
    </div>
  );
}
