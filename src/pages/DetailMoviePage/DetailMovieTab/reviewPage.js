import React from "react";
import { Media } from "reactstrap";

export default function reviewPage() {
  return (
    <div className="mt-3 detail-page-content">
      <Media className="mt-1">
        <Media className="mr-3" left middle href="#">
          <img
            className="img-card-review"
            src="https://www.themoviedb.org/t/p/w138_and_h175_face/amOshiwsbyIyvkhm9QK48xuafyH.jpg"
            alt="Generic placeholder"
          />
        </Media>
        <Media body>
          <Media heading>rate</Media>
          <textarea
            className="form-control"
            placeholder="leave comment here"
          ></textarea>
        </Media>
      </Media>
      <div>
        <Media className="mt-3">
          <Media className="mr-3" left middle href="#">
            <img
              className="img-card-review"
              src="https://www.themoviedb.org/t/p/w138_and_h175_face/amOshiwsbyIyvkhm9QK48xuafyH.jpg"
              alt="Generic placeholder"
            />
          </Media>
          <Media body>
            <Media heading>Middle aligned media</Media>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
            scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum
            in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac
            nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
          </Media>
        </Media>
      </div>
    </div>
  );
}
