import { combineReducers } from "redux";
import homePageReducer from "./reducers/HomePage.js";

export default combineReducers({
  homePage: homePageReducer,
});
