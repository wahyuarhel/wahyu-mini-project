export const movieUrl = "https://api.themoviedb.org/3/";
export const imgUrl = "https://image.tmdb.org/t/p/w1280";
export const apiKey = "api_key=1bced6f7798c466e3e00b828cc655b0f";
export const nowPlaying =
  "movie/now_playing?api_key=1bced6f7798c466e3e00b828cc655b0f";
export const genreListUrl =
  "https://api.themoviedb.org/3/genre/movie/list?api_key=1bced6f7798c466e3e00b828cc655b0f&language=en-US";
export const moviebyGenreUrl =
  "https://api.themoviedb.org/3/movie/popular?api_key=1bced6f7798c466e3e00b828cc655b0f&with_genres=";

export const items = [
  {
    src: "https://image.tmdb.org/t/p/w1280/lOSdUkGQmbAl5JQ3QoHqBZUbZhC.jpg",
    altText: "Slide 1",
    key: "1",
  },
  {
    src: "https://image.tmdb.org/t/p/w1280/srYya1ZlI97Au4jUYAktDe3avyA.jpg",
    altText: "Slide 2",
    key: "2",
  },
  {
    src: "https://image.tmdb.org/t/p/w1280/3ombg55JQiIpoPnXYb2oYdr6DtP.jpg",
    altText: "Slide 3",
    key: "3",
  },
];
